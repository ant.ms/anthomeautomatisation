# import dependencies
from flask import Flask, request
from flask_restful import Resource, Api
import psutil
from flask_cors import CORS

# import APIs
from apis.magicLight import MagicHomeApi


# region: Logging

def log(level, message):
    print(f'[{level}] {message}')

# endregion

# region magicHome

app = Flask(__name__)
cors = CORS(app, resources={r"*": {"origins": "*"}})
api = Api(app)

class magicHomePower(Resource):
    def get(self):
        args = request.args

        if args['power'] == '1':
            MagicHomeApi(args['ip'], 3).turn_on()
        else:
            MagicHomeApi(args['ip'], 3).turn_off()

        log(0, f"turning state of Magic Bulb {args['ip']} to {args['power']}")
        return f"turning state of Magic Bulb {args['ip']} to {args['power']}"

class magicHomeWhite(Resource):
    def get(self):
        args = request.args

        MagicHomeApi(args['ip'], 3).update_device(0, 0, 0, int(args['brightness']))

        log(0, f"turning Magic Bulb {args['ip']} to  white with {args['brightness']/2.5}% brightness")
        return f"turning Magic Bulb {args['ip']} to  white with {args['brightness']/2.5}% brightness"

class magicHomeColor(Resource):
    def get(self):
        args = request.args

        MagicHomeApi(args['ip'], 3).update_device(int(args['r']), int(args['g']), int(args['b']))

        log(0, f"turning color of Magic Bulb {args['ip']} to {args['r']},{args['g']},{args['b']}")
        return f"turning color of Magic Bulb {args['ip']} to {args['r']},{args['g']},{args['b']}"

api.add_resource(magicHomePower, '/action/magic/power')
api.add_resource(magicHomeColor, '/action/magic/color')
api.add_resource(magicHomeWhite, '/action/magic/white')

# endregion magicHome

if __name__ == '__main__':
    log(1, "starting webserver")
    app.run(port='6969', host='0.0.0.0')